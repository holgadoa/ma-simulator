# The value of Minimal Information Exchange in Multi-Agent Reinforcement


This repository contains the code of The value of Minimal Information Exchange in Multi-Agent Reinforcement learning Msc Thesis

## Abstract





## Setup
### Prerequisites
* The code is tested with Python 3.7, PyTorch 0.4.1 and Ubuntu 18.04. 


### Hardware setup
The model was trained and tested in a machine with the following features:
 * Nvidia GTX 1050
 * 16 GB RAM
 * 9 CPU cores

### Simulator
Our algorithms were tested using our in-house developed simulator that you can find at src/enviroments/enviroments_2.py
Our Simulator emulates the behaviour of a multi-server cluster

The cluster is componed by N servers that are connected via a communication graph.

The communication graph is undirected and circular

Each agent will received messages from its direct neighboughs

The simulator can be configure with the following parameters:

 * number_servers (Int), "number of servers to be deployed in the Environment" 
 * max_size_input_queue (Int), "Max number of packages that can be hosted in the server' input queues"
 * livespan (Int), "packages' livespan"
 * memory (Int), "server processing Memory"
 * speed: (Int), "server processing speed"
 * pckg_size: tupple(Int, Int) "Range for packages size"
 * max_priority (Int), "Stablish the packages priority levels"
 * max_env_traffic (Int), "Max number of packages to be processed"
 * max_discard_fc (Float), "Percentage of the incoming traffic that can get expired" 
 * success_factor (Float), "Value to control the traffic Volume" 1 is max traffic volume and 0 is no traffic
 * logs (Bool), "Shows debugging information"
 * info_in_observation (Int), "Size of KPIs to be retrieve from the enviroment"
 * support_servers (Bool), "Enable/Disable option of Support Servers"
 * support_server_memory: (Int) "Support Servers memory capacity"
 * support_server_speed: (Int) "Support servers processing speed"
 * number_of_support_servers list(Int) "List that contains the support servers IDs"

### Example of calling the test Environment
 
```bash

opt = Options(number_servers=5,
              max_size_input_queue=10,
              livespan=15.,
              memory=50,
              speed=2,
              pckg_size=(30, 50),
              max_priority=3,
              max_env_traffic=200,
              max_discard_fc=0.1,
              success_factor=0.3,
              logs=False,
              info_in_observation=5,
              support_servers=True,
              support_server_memory=150,
              support_server_speed=10,
              number_of_support_servers=[0, 2])

env = MultiServerEnvironment(opt)

```


### Training Phase

```bash
python Algorithm_name/train.py # Please substitute Algorithm_name with one of the proposed options below
```

the availble algorithms are:
* centralice_learning
* centralice_learning_COMA
* Independent_COM_learning
* Independent_nonCom_learning
* clustered_learning

All the algorithms are based on Actor Critics Architectures

* future development: "A argument parser will be include for hyper parameter tunning and visualization"

## Authors
**Jose Luis Holgado Alvarez**

## Acknowledgements
This software was developed as part of Internship hosted by Eurecom and Huawei



## Maintained by 
**Jose Luis Holgado Alvarez**
 
 E-mail:
  * holgadoa@eurecom.fr
  * jlhalv92@gmail.com

## License
The code in this repository to facilitate the use of the `The value of Minimal Information Exchange in Multi-Agent Reinforcement` algorithms is licensed under the **MIT License**:

```
MIT License

Copyright (c) 2020 The Authors of The Paper, " S2-cGAN: Self-Supervised Adversarial Representation Learning for Binary Change Detection in Multispectral Images"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```



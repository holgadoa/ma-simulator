"""
Training loop for the Coma framework on Switch2-v0 the ma_gym
"""


from src.enviroment.enviroment_v2 import *

import numpy as np
from COMA import *
from tensorboardX import SummaryWriter

def moving_average(x, N):
    return np.convolve(x, np.ones((N,)) / N, mode='valid')


if __name__ == "__main__":

    states = [0,1,2 ,3,4,5,6,7,8]
    # states = [ 1, 2, 3, 5, 6]
    logs = "/home/joseluis/Projects/ma-simulator/src/algorithms/runs/COMA_{}"
    # length_msg = f"Support_servers_short_space_{len(states)}_pentalty_full"
    length_msg = "test_2_support_servers"

    writer = SummaryWriter(logs.format(length_msg))
    # Hyperparameters
    agent_num = 5

    state_dim = len(states)
    action_dim = 4

    gamma = 0.99
    lr_a = 0.0005
    lr_c = 0.0005
    clip = 10
    # lr_a = 0.0005
    # lr_c = 0.0005

    target_update_steps = 100
    train_step = 10
    # agent initialisation

    agents = COMA(agent_num, state_dim, action_dim, lr_c, lr_a, gamma, target_update_steps, clip)
    # opt = Options()

    opt = Options(number_servers=5,
                  max_size_input_queue=10,
                  livespan=15.,
                  memory=50,
                  speed=2,
                  pckg_size=(30, 50),
                  max_priority=3,
                  max_env_traffic=200,
                  max_discard_fc=0.1,
                  success_factor=0.3,
                  logs=False,
                  info_in_observation=5,
                  support_servers=True,
                  support_server_memory=150,
                  support_server_speed=10,
                  number_of_support_servers=[0, 2])

    env = MultiServerEnvironment(opt)
    obs = env.reset()
    obs = obs[:,0,states]


    episode_reward = 0
    episodes_reward = []

    # training loop
    actions_histogram = [[], [], [], [], []]
    n_episodes = 10000
    episode = 0
    print("agents ids: ", env.agent_index_list, f"episode: {episode}")
    msgs = np.ones((5,9))
    steps = 0
    mean_departured_pkgs = []
    departure_pkgs = 0
    while episode < n_episodes and departure_pkgs < 200 :

        memory = []
        actions = agents.get_actions(obs)
        for i in env.agent_index_list:
            actions_histogram[i].append(actions[i])

        next_obs, reward, done_n, info = env.step(actions,msgs)
        departure_pkgs = info[5]



        next_obs = next_obs[:,0,states]
        agents.memory.reward.append(reward)
        for i in range(agent_num):
            agents.memory.done[i].append(done_n)

        episode_reward += sum(reward)
        # print(reward.sum())
        mean_departured_pkgs.append(info[5])
        mean_departured_pkgs_value = np.array(mean_departured_pkgs[-10:]).mean()
        obs = next_obs
        memory.append(obs[:, 3])

        if done_n:
            episodes_reward.append(episode_reward)
            avg_reward = np.array(episodes_reward[-10:])
            print(
                f"steps_episode:{steps} episode: {episode}, average reward: {avg_reward.mean()}, episode reward: {episode_reward}")
            steps = 0


            # print(episodes_reward)

            # for i in env.agent_index_list:
            #
            #     writer.add_scalar(f"processing memory free agent {i}",obs[i,0], episode)
            #     writer.add_scalar(f"num_departs agent {i}", obs[i,1], episode)
            #     writer.add_scalar(f"total_expired agent {i}", obs[i,2], episode)
            #     writer.add_scalar(f"total_reroute agent {i}", obs[i,3], episode)



            episode += 1

            obs = env.reset()

            obs = obs[:,0,states]
            if episode % train_step == 0:
                critic_loss, actor_loss = agents.train()

                for agent_id in env.agent_index_list:
                    writer.add_scalar("total_loss_agent{}".format(agent_id),
                                      critic_loss + actor_loss[agent_id],
                                  episode)

                for agent_id in env.agent_index_list:
                    writer.add_scalar("actor_loss_agent{}".format(agent_id),
                                      actor_loss[agent_id],
                                      episode)

                writer.add_scalar("critic loss",
                                  critic_loss,
                                  episode)

            if episode % 1 == 0:





                episode_reward = 0
                print("Number of packages delivered", info[5])
                writer.add_scalar("mean_reward", avg_reward.mean(), episode)
                writer.add_scalar("episode_reward", episode_reward, episode)
                writer.add_scalar("Mean total number of departured pkgs", mean_departured_pkgs_value, episode)
                writer.add_scalar("total number of departured pkgs", info[5], episode)
                writer.add_scalar("total number of refused pkgs", info[4], episode)
                writer.add_scalar("total number of expired pkgs", info[2], episode)
                writer.add_scalar("total number of rerouted pkgs", info[3], episode)
                memory_np = np.array(memory)
                for agent_id in env.agent_index_list:
                    writer.add_scalar("total_memory_used_agent_{}".format(agent_id),
                                      100 - 100 * memory_np[:, agent_id].mean(), episode)

                for agent_id in env.agent_index_list:
                    writer.add_histogram("action histogram agent {}".format(agent_id),
                                         np.array(actions_histogram[agent_id])
                                         , global_step=episode)

                actions_histogram = [[], [], [], [], []]

        steps += 1



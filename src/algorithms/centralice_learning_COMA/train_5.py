"""
Training loop for the Coma framework on Switch2-v0 the ma_gym
"""
from src.enviroment.enviroment_v2 import *

import numpy as np
from COMA import *
from tensorboardX import SummaryWriter

def moving_average(x, N):
    return np.convolve(x, np.ones((N,)) / N, mode='valid')


if __name__ == "__main__":

    agent_num = 5

    state_dim = 9
    action_dim = 4

    gamma = 0.99
    lr_a = 0.0001
    lr_c = 0.005

    target_update_steps = 10

    # agent initialisation

    agents = COMA(agent_num, state_dim, action_dim, lr_c, lr_a, gamma, target_update_steps)

    opt = Options(number_servers=5,
                  max_size_input_queue=10,
                  livespan=15.,
                  memory=50,
                  speed=2,
                  pckg_size=(30, 50),
                  max_priority=3,
                  max_env_traffic=200,
                  max_discard_fc=0.1,
                  success_factor=0.3,
                  logs=False,
                  info_in_observation=5,
                  support_servers=True,
                  support_server_memory=150,
                  support_server_speed=10,
                  number_of_support_servers=[0, 2])

    env = MultiServerEnvironment(opt)
    obs = env.reset()
    obs = obs[:, 0, :]

    episode_reward = 0
    episodes_reward = []

    n_episodes = 10000
    episode = 0

    while episode < n_episodes:
        actions = agents.get_actions(obs)
        msg = np.zeros((5, 9))
        next_obs, reward, done_n, info = env.step(actions, msg)

        agents.memory.reward.append(reward)
        for i in range(agent_num):
            agents.memory.done[i].append(done_n)

        episode_reward += sum(reward)
        obs = next_obs[:, 0, :]

        if done_n:

            episodes_reward.append(episode_reward)
            episode_reward = 0

            episode += 1

            obs = env.reset()
            obs = obs[:, 0, :]

            if episode % 10 == 0:
                print(
                    f"episode: {episode}, average reward: {sum(episodes_reward[-10:]) / 10}, departure pkgs: {info[5]},  expired pkgs: {info[2]}, reward: {episodes_reward[-1]}, expected_reward: {info[5] - info[2]}")
                agents.train()

            if episode % 100 == 0:
                print(f"episode: {episode}, average reward: {sum(episodes_reward[-100:]) / 100}")
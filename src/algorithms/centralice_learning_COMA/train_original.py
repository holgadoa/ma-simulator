
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Categorical
from src.enviroment.enviroment import *
from tensorboardX import SummaryWriter

class Memory:
    def __init__(self, agent_num, action_dim):
        self.agent_num = agent_num
        self.action_dim = action_dim

        self.actions = []
        self.observations = []
        self.pi = [[] for _ in range(agent_num)]
        self.reward = []
        self.done = [[] for _ in range(agent_num)]

    def get(self):
        actions = torch.tensor(self.actions)
        observations = self.observations

        pi = []
        for i in range(self.agent_num):
            pi.append(torch.cat(self.pi[i]).view(len(self.pi[i]), self.action_dim))

        reward = torch.tensor(self.reward)
        done = self.done

        return actions, observations, pi, reward, done

    def clear(self):
        self.actions = []
        self.observations = []
        self.pi = [[] for _ in range(agent_num)]
        self.reward = []
        self.done = [[] for _ in range(agent_num)]


class Actor(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Actor, self).__init__()

        self.fc1 = nn.Linear(state_dim, 64)
        self.fc2 = nn.Linear(64, 64)
        self.fc3 = nn.Linear(64, action_dim)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        return F.softmax(self.fc3(x), dim=-1)

class Critic(nn.Module):
    def __init__(self, agent_num, state_dim, action_dim):
        super(Critic, self).__init__()

        input_dim = 1 + state_dim * agent_num + agent_num

        self.fc1 = nn.Linear(input_dim, 64)
        self.fc2 = nn.Linear(64, 64)
        self.fc3 = nn.Linear(64, action_dim)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        return self.fc3(x)

class COMA:
    def __init__(self, agent_list, state_dim, action_dim, lr_c, lr_a, gamma, target_update_steps, device):
        self.agent_list = agent_list
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.device = device
        self.gamma = gamma

        self.target_update_steps = target_update_steps

        self.memory = Memory(len(agent_list), action_dim)

        self.actors = [Actor(state_dim, action_dim).to(device) for _ in agent_list]
        self.critic = Critic(agent_num, state_dim, action_dim).to(device)

        self.critic_target = Critic(len(agent_list), state_dim, action_dim).to(device)
        self.critic_target.load_state_dict(self.critic.state_dict())

        self.actors_optimizer = [torch.optim.Adam(self.actors[i].parameters(), lr=lr_a) for i in agent_list]
        self.critic_optimizer = torch.optim.Adam(self.critic.parameters(), lr=lr_c)

        self.count = 0

    def get_actions(self, observations):
        observations = torch.tensor(observations)

        actions = []

        for i in self.agent_list:
            dist = self.actors[i](observations[i].to(device))
            action = Categorical(dist).sample()

            self.memory.pi[i].append(dist)
            actions.append(action.item())

        self.memory.observations.append(observations)
        self.memory.actions.append(actions)

        return actions

    def train(self):
        actor_optimizer = self.actors_optimizer
        critic_optimizer = self.critic_optimizer

        actions, observations, pi, reward, done = self.memory.get()

        for i in self.agent_list:
            # train actor

            input_critic = self.build_input_critic(i, observations, actions)
            Q_target = self.critic_target(input_critic.to(self.device)).detach()

            action_taken = actions.type(torch.long)[:, i].reshape(-1, 1).to(self.device)

            baseline = torch.sum(pi[i] * Q_target, dim=1).detach()
            Q_taken_target = torch.gather(Q_target, dim=1, index=action_taken).squeeze()
            advantage = Q_taken_target - baseline

            log_pi = torch.log(torch.gather(pi[i], dim=1, index=action_taken).squeeze())

            actor_loss = - torch.mean(advantage * log_pi)

            actor_optimizer[i].zero_grad()
            actor_loss.backward()
            torch.nn.utils.clip_grad_norm_(self.actors[i].parameters(), 5)
            actor_optimizer[i].step()

            # train critic

            Q = self.critic(input_critic.to(self.device))

            action_taken = actions.type(torch.long)[:, i].reshape(-1, 1).to(self.device)
            Q_taken = torch.gather(Q, dim=1, index=action_taken).squeeze()

            # TD(0)
            r = torch.zeros(len(reward[:, i])).to(self.device)
            for t in range(len(reward[:, i])):
                if done[i][t]:
                    r[t] = reward[:, i][t]
                else:
                    r[t] = reward[:, i][t] + self.gamma * Q_taken_target[t + 1]

            critic_loss = torch.mean((r - Q_taken) ** 2)

            critic_optimizer.zero_grad()
            critic_loss.backward()
            torch.nn.utils.clip_grad_norm_(self.critic.parameters(), 5)
            critic_optimizer.step()

        if self.count == self.target_update_steps:
            self.critic_target.load_state_dict(agents.critic.state_dict())
            self.count = 0
        else:
            self.count += 1

        self.memory.clear()

    def build_input_critic(self, agent_id, observations, actions):
        batch_size = len(observations)

        ids = (torch.ones(batch_size) * agent_id).view(-1, 1)

        observations = torch.cat(observations).view(batch_size, self.state_dim * len(self.agent_list))
        input_critic = torch.cat([observations.type(torch.float32), actions.type(torch.float32)], dim=-1)
        input_critic = torch.cat([ids, input_critic], dim=-1)

        return input_critic

def moving_average(x, N):
    return np.convolve(x, np.ones((N,)) / N, mode='valid')




if __name__ == "__main__":
    # Hyperparameters
    agent_num = 5
    state_dim = 9
    action_dim = 5
    gamma = 0.99
    lr_a = 0.0001
    lr_c = 0.005
    device = torch.device(0)
    target_update_steps = 10
    logs = "/home/joseluis/Projects/ma-simulator/src/algorithms/runs/COMA_Centraliced_Learning_{}"
    lengh_msg = "v5"
    mean_departured_pkgs = []
    writer = SummaryWriter(logs.format(lengh_msg))


    opt = Options(number_servers=5,
                  max_size_input_queue=10,
                  livespan=15.,
                  memory=50,
                  speed=2,
                  pckg_size=(30, 50),
                  max_priority=3,
                  max_env_traffic=200,
                  max_discard_fc=0.1,
                  success_factor=0.3,
                  logs=False,
                  info_in_observation=5,
                  support_servers=True,
                  support_server_memory=150,
                  support_server_speed=10,
                  number_of_support_servers=[0, 2])
    env = MultiServerEnvironment(opt)
    # agent initialisation
    agents = COMA(env.agent_index_list, state_dim, action_dim, lr_c, lr_a, gamma, target_update_steps, device)
    obs = env.reset()[:,0,:]
    msgs = np.zeros((5,9))
    episode_reward = 0
    episodes_reward = []

    # training loop

    n_episodes = 10000
    episode = 0

    while episode < n_episodes:
        obs = env.reset()[:, 0, :]
        print("Reset Enviroment")
        COMA.agent_list = env.agent_index_list
        done_n = False
        actions_histogram = [[], [], [], [], []]
        n_steps = 1000
        steps = 0
        episode_reward = 0
        memory = []
        print("---------------episode: ", episode, "----------------------------")
        while not done_n:
            actions = agents.get_actions(obs)
            for i in env.agent_index_list:
                actions_histogram[i].append(actions[i])
            next_obs, reward, done_n, info = env.step(actions, msgs)
            mean_departured_pkgs.append(info[5])
            next_obs = next_obs[:,0,:]
            # print("########## step: ", steps)
            # print("Number of packages delivered", info[5],next_obs[:,-4].sum())
            # print("Number of packages refused pkgs", info[4], next_obs[:,-1].sum())
            # print("Number of packages expired pkgs", info[2],next_obs[:,-3].sum())
            memory.append(next_obs[:, 3])
            agents.memory.reward.append(reward)
            for i in env.agent_index_list:
                agents.memory.done[i].append(done_n)

            episode_reward += sum(reward)
            obs = next_obs
            steps += 1

        # Reset Episode
        print("Number of packages delivered", info[5],next_obs[:,-4].sum())
        print("Number of packages refused pkgs", info[4], next_obs[:,-1].sum())
        print("Number of packages expired pkgs", info[2],next_obs[:,-3].sum())
        episodes_reward.append(episode_reward)
        writer.add_scalar("Episode Rewards", episode_reward, episode)

        for i in env.agent_index_list:
            agents.memory.done[i].append(done_n)

        for agent_id in env.agent_index_list:
            writer.add_histogram("action histogram agent {}".format(agent_id),
                                 np.array(actions_histogram[agent_id])
                                 ,global_step=episode)

        if episode % 1 == 0:
            agents.train()

            writer.add_scalar("total number of refused pkgs", info[4], episode)
            writer.add_scalar("total number of expired pkgs", info[2], episode)
            writer.add_scalar("total number of rerouted pkgs", info[3], episode)
            writer.add_scalar("total number of departured pkgs", info[5], episode)
            memory_np = np.array(memory)

            for agent_id in env.agent_index_list:
                writer.add_scalar("total_memory_used_agent_{}".format(agent_id), 100-100*memory_np[:, agent_id].mean(),
                                  episode)

        if episode % 50 == 0:
            print(f"episode: {episode}, average reward: {sum(episodes_reward[-50:]) / 50}")
            writer.add_scalar("mean_reward", sum(episodes_reward[-50:]) / 50, episode)
            mean_departured_pkgs_value = np.array(mean_departured_pkgs[-50:]).mean()
            writer.add_scalar("Mean total number of departured pkgs", mean_departured_pkgs_value, episode)

        episode += 1




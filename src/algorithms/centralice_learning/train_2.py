import argparse
import copy
import os
from itertools import count
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Categorical
from src.enviroment.enviroment_v2 import *
from tensorboardX import SummaryWriter
import torch.nn.utils as nn_utils
from scipy.special import softmax
import numpy as np

ENTROPY_BETA = 0.01
CLIP_GRAD = 0.1
parser = argparse.ArgumentParser(description='PyTorch actor-critic')
parser.add_argument('--gamma', type=float, default=0.99, metavar='G',
                    help='discount factor (default: 0.99)')
parser.add_argument('--lrate', type=float, default=0.0008, metavar='G',
                    help='learning rate (default: 0.003)')
parser.add_argument('--seed', type=int, default=543, metavar='N',
                    help='random seed (default: 543)')
parser.add_argument('--render', action='store_true',
                    help='render the environment')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='interval between training status logs (default: 10)')
args = parser.parse_args()


torch.manual_seed(args.seed)
opt = Options(number_servers=5,
              max_size_input_queue=10,
              livespan=15.,
              memory=50,
              speed=2,
              pckg_size=(30, 50),
              max_priority=3,
              max_env_traffic=200,
              max_discard_fc=0.1,
              success_factor=0.3,
              logs=False,
              info_in_observation=5,
              support_servers=True,
              support_server_memory=150,
              support_server_speed=10,
              number_of_support_servers=[0, 2])

# opt = Options()
env = MultiServerEnvironment(opt)
torch.manual_seed(args.seed)
device = torch.device(0)


class Critic(nn.Module):
    """
    implements both actor and critic in one model
    """
    def __init__(self):
        super(Critic, self).__init__()
        self.affine1 = nn.Linear(5 * 9, 128)
        self.flatten = nn.Flatten()

        # values buffer
        self.saved_values = []

        # critic's layer
        self.value_head = nn.Linear(128, 1)

    def forward(self, x):
        """
        forward of both actor and critic
        """
        x = x.flatten()
        x = F.relu(self.affine1(x))

        # critic: evaluates being in the state s_t
        state_values = self.value_head(x)


        # return values for both actor and critic as a tuple of 2 values:
        # 1. a list with the probability of each action over the action space
        # 2. the value from state s_t
        return state_values


class Policy(nn.Module):
    """
    implements both actor and critic in one model
    """
    def __init__(self):
        super(Policy, self).__init__()
        self.affine1 = nn.Linear(5 * 9, 128)
        self.flatten = nn.Flatten()

        # actor's layer
        self.action_head = nn.Linear(128, 4)

        # action & reward buffer
        self.saved_actions = []
        self.rewards = []

    def forward(self, x):
        """
        forward of both actor and critic
        """
        x = x.flatten()
        x = F.relu(self.affine1(x))

        # actor: choses action to take from state s_t
        # by returning probability of each action
        action_prob = F.softmax(self.action_head(x), dim=-1)

        # critic: evaluates being in the state s_t

        # return values for both actor and critic as a tuple of 2 values:
        # 1. a list with the probability of each action over the action space
        # 2. the value from state s_t
        return action_prob


Central_Critic = Critic()
agents = [[Policy().to(device), copy.deepcopy(Central_Critic).to(device)] for _ in range(opt.number_servers)]
optimizers = [[optim.Adam(agents[i][0].parameters(), lr=args.lrate),
               optim.Adam(agents[i][1].parameters(), lr=args.lrate)] for i in range(opt.number_servers)]

eps = np.finfo(np.float32).eps.item()


def select_action(state, agent_net):


    actor = agent_net[0]
    critic = agent_net[1]
    state = torch.from_numpy(state).float().to(device)
    probs = actor(state)
    state_value = critic(state)
    # create a categorical distribution over the list of probabilities of actions
    m = Categorical(probs)

    # and sample an action using the distribution
    action = m.sample()


    # save to action buffer
    actor.saved_actions.append(m.log_prob(action).to(device))
    critic.saved_values.append(state_value.to(device))
    # the action to take (left or right)
    return action.item()


def finish_episode(agent, optimizers):
    """
    Training code. Calculates actor and critic loss and performs backprop.
    """
    R = 0
    optimizer_actor = optimizers[0]
    optimizer_critic = optimizers[1]
    actor = agent[0]
    critic = agent[1]
    saved_actions = actor.saved_actions
    saved_values = critic.saved_values
    policy_losses = [] # list to save actor (policy) loss
    value_losses = [] # list to save critic (value) loss
    returns = [] # list to save the true values

    # calculate the true value using rewards returned from the environment
    for r in actor.rewards[::-1]:
        # calculate the discounted value
        R = r + args.gamma * R
        returns.insert(0, R)

    advantages = []
    entropy_loss_v = []

    returns = torch.tensor(returns)
    returns = (returns - returns.mean()) / (returns.std() + eps)
    returns = returns.detach()
    for log_prob, value, R in zip(saved_actions, saved_values, returns):
        advantages.append(R - value)

    advantages = torch.tensor(advantages).to(device).detach()
    # calculate actor (policy) loss
    for log_prob, value, R, advantage in zip(saved_actions, saved_values, returns, advantages):
        log_prob_actions_v = log_prob * advantage
        policy_losses.append(-log_prob * advantage)
        entropy_loss_v.append(ENTROPY_BETA * (log_prob * log_prob_actions_v))
        # calculate critic (value) loss using L1 smooth loss
        value_losses.append(F.smooth_l1_loss(value, torch.tensor([R]).to(device).detach()))

    entropy_loss_v = torch.tensor(entropy_loss_v).to(device).detach()
    # perform backprop actor only
    optimizer_actor.zero_grad()
    # sum up all the values of policy_losses
    loss_actor = torch.stack(policy_losses).sum()
    loss_actor.backward()
    nn_utils.clip_grad_norm_(actor.parameters(), CLIP_GRAD)
    optimizer_actor.step()


    # reset gradients
    optimizer_critic.zero_grad()
    # sum up all the values of value_losses

    loss_critic = torch.stack(value_losses).sum() + entropy_loss_v.sum()
    loss_critic.backward()
    nn_utils.clip_grad_norm_(critic.parameters(), CLIP_GRAD)
    optimizer_critic.step()



    # reset rewards and action buffer
    del actor.rewards[:]
    del actor.saved_actions[:]
    del critic.saved_values[:]
    return loss_actor.data, loss_critic.data


def calculate_graph_consensus(agents, neighbors_list):
    agent = 0
    c = np.zeros(len(neighbors_list))
    cii = 0
    for j in range(len(agents)):
        if j == agent:
            for i in range(1, len(agents)):
                d0 = len(neighbors_list[i])
                d1 = len(neighbors_list[agent])
                ci = 1/(1 + max(d1, d0))
                cii += ci

            c[j] = 1 - cii
        else:
            d0 = len(neighbors_list[j])
            d1 = len(neighbors_list[agent])
            c[j] = 1/(1 + max(d1, d0))

    return c


def consensus_step(cgraph, creward, parameters):
    c = softmax(cgraph + creward)
    parameter = np.sum(c*parameters)
    return parameter


def consensus_parameters(agent_index_consesus, neighbours_consesus, rewards_consensus):

    cgraph = calculate_graph_consensus(agent_index_consesus, neighbours_consesus)
    creward = rewards_consensus/np.abs(rewards_consensus.sum() + 1e-20)
    creward = softmax(creward)
    return cgraph, creward


def main():
    mean_departured_pkgs = []
    actions_histogram = [[], [], [], [], []]
    logs = "/home/joseluis/Projects/ma-simulator/src/algorithms/runs/Centraliced_Learning_{}"
    lengh_msg = "Support_servers_0.0008_lr"
    checkpoint = "/home/joseluis/Projects/ma-simulator/src/algorithms"
    losses_actor = np.zeros(len(env.agent_index_list))
    losses_critic = np.zeros(len(env.agent_index_list))
    writer = SummaryWriter(logs.format(lengh_msg))
    running_reward = np.ones(len(env.agent_index_list))
    actions = np.zeros(len(env.agent_index_list))
    #Message structure
    "mgs = [number_of_high_priority," \
    "machine status," \
    "input queue size," \
    " processing memory available, " \
    "num_arrivals," \
    "num_departs," \
    "total_expired," \
    "total_reroute," \
    "total_refuse]"

    msgs = np.zeros((len(env.agent_index_list), 9))

    t = 0
    info = np.zeros(7)
    # run inifinitely many episodes
    for i_episode in count(1):

        # reset environment and episode reward
        print("reset enviroment")
        state = env.reset()
        state = state[:,0,:]
        ep_reward = np.zeros(len(env.agent_index_list))

        # for each episode, only run 9999 steps so that we don't
        # infinite loop while learning
        for t in range(1, 10000):

            for i in env.agent_index_list:

                # select action from policy
                actions[i] = select_action(state, agents[i])
                actions_histogram[i].append(actions[i])
                # take the action
            state, reward, done, info = env.step(actions, msgs)
            state = state[:, 0, :]

            for i in env.agent_index_list:
                agents[i][0].rewards.append(float(reward[i]))

            ep_reward += reward

            if done:
                break

        # update cumulative reward
        running_reward = 0.05 * ep_reward + (1 - 0.05) * running_reward

        # perform backdrop
        for i in env.agent_index_list:
            losses_actor[i], losses_critic[i] = finish_episode(agents[i], optimizers[i])


        #perform Critic Consesus
        # Average centralices
        for i in env.agent_index_list:
            params_neighbours = [agents[i][1].state_dict()]
            beta = 1/len(env.agent_index_list)
            new_critic = Critic().to(device)
            new_critic_dict = new_critic.state_dict()
            for key in params_neighbours[0]:
                for params in params_neighbours:
                    new_critic_dict[key] += beta*params[key]

            for j in env.agent_index_list:
                agents[j][1].load_state_dict(new_critic_dict)

        mean_departured_pkgs.append(info[5])
        # log results
        if i_episode % args.log_interval == 0:
            print('Episode {}\tsteps: {}\tLast reward: {:.2f}\tAverage reward: {:.2f}'.format(
                  i_episode, t, ep_reward.sum(), running_reward.sum()))
            writer.add_scalar("mean_reward", running_reward.sum(), i_episode)
            writer.add_scalar("episode_reward", ep_reward.sum(), i_episode)
            for agent_id in env.agent_index_list:

                writer.add_scalar("total_loss_agent{}".format(agent_id),
                                  losses_actor[agent_id]+losses_critic[agent_id],
                                  i_episode)

        mean_departured_pkgs_value = np.array(mean_departured_pkgs[-10:]).mean()
        print("Stop condition: MAX Packages", opt.max_env_traffic)
        print("Number of packages delivered", info[5])
        print("10 mean Number of packages delivered", mean_departured_pkgs_value)
        # check if we have "solved" the servers challenge

        if i_episode > 10000 or mean_departured_pkgs_value >= opt.max_env_traffic:
            print("Solved! Running reward is now {} and "
                  "the last episode old_runs to {} time steps!".format(running_reward.sum(), t))

            if not os.path.isdir(checkpoint + "/checkpoints_{}".format(lengh_msg)):

                os.mkdir(checkpoint + "/checkpoints_{}".format(lengh_msg))

            for agent_id in env.agent_index_list:
                model_path = checkpoint + "/checkpoints_{}".format(lengh_msg)
                torch.save(
                    agents[agent_id][0].state_dict(),
                    model_path + "clustered_learning_actor_agent_{}.pth".format(agent_id))
                torch.save(
                    agents[agent_id][1].state_dict(),
                    model_path + "clustered_learning_critic_agent_{}.pth".format(agent_id))

            writer.add_scalar("mean_reward", running_reward.sum(), i_episode)
            writer.add_scalar("episode_reward", ep_reward.sum(), i_episode)
            writer.add_scalar("Mean total number of departured pkgs", mean_departured_pkgs_value, i_episode)
            writer.add_scalar("total number of departured pkgs", info[5], i_episode)
            writer.add_scalar("total number of refused pkgs", info[4], i_episode)
            writer.add_scalar("total number of expired pkgs", info[2], i_episode)
            writer.add_scalar("total number of rerouted pkgs", info[3], i_episode)

            for agent in env.agent_index_list:

                writer.add_scalar("loss_agent{}".format(agent), losses_actor[agent] + losses_critic[agent], i_episode)

            writer.close()


            break


        writer.add_scalar("Mean total number of departured pkgs", mean_departured_pkgs_value, i_episode)
        writer.add_scalar("total number of departured pkgs", info[5], i_episode)
        writer.add_scalar("total number of refused pkgs", info[4], i_episode)
        writer.add_scalar("total number of expired pkgs", info[2], i_episode)
        writer.add_scalar("total number of rerouted pkgs", info[3], i_episode)


        for agent_id in env.agent_index_list:

            writer.add_histogram("action histogram agent {}".format(agent_id),
                                 np.array(actions_histogram[agent_id])
                                 ,global_step=i_episode)

        actions_histogram = [[],[],[],[],[]]


if __name__ == '__main__':
    main()
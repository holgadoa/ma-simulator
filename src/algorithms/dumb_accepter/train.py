
from itertools import count
from src.enviroment.enviroment_v2 import *
from tensorboardX import SummaryWriter

import numpy as np

# opt = Options()
# opt = Options(number_servers=5,
#               max_size_input_queue=10,
#               livespan=15.,
#               memory=100,
#               speed=10,
#               pckg_size=(30, 50),
#               max_priority=3,
#               max_env_traffic=200,
#               max_discard_fc=0.1,
#               success_factor=0.3,
#               logs=False,
#               info_in_observation=5,
#               support_servers=False,
#               support_server_memory=150,
#               support_server_speed=10,
#               number_of_support_servers=[0, 2])

# opt = Options(number_servers=5,
#               max_size_input_queue=10,
#               livespan=10.,
#               memory=100,
#               speed=10,
#               pckg_size=(30, 50),
#               max_priority=3,
#               max_env_traffic=200,
#               max_discard_fc=0.1,
#               success_factor=0.6,
#               logs=False,
#               info_in_observation=5,
#               support_servers=False,
#               support_server_memory=150,
#               support_server_speed=10,
#               number_of_support_servers=[0, 2])

# opt = Options(number_servers=5,
#               max_size_input_queue=10,
#               livespan=15.,
#               memory=50,
#               speed=2,
#               pckg_size=(30, 50),
#               max_priority=3,
#               max_env_traffic=200,
#               max_discard_fc=0.1,
#               success_factor=0.3,
#               logs=False,
#               info_in_observation=5,
#               support_servers=True,
#               support_server_memory=150,
#               support_server_speed=10,
#               number_of_support_servers=[0, 2])

opt = Options(number_servers=5,
              max_size_input_queue=10,
              livespan=15.,
              memory=50,
              speed=2,
              pckg_size=(30, 50),
              max_priority=3,
              max_env_traffic=200,
              max_discard_fc=0.1,
              success_factor=0.3,
              logs=False,
              info_in_observation=5,
              support_servers=True,
              support_server_memory=150,
              support_server_speed=10,
              number_of_support_servers=[0, 2])

env = MultiServerEnvironment(opt)




def main():
    actions_histogram = [[],[],[],[],[]]
    mean_departured_pkgs = []
    logs = "/home/joseluis/Projects/ma-simulator/src/algorithms/runs/DUMB_{}"
    lengh_msg = "support_servers"

    writer = SummaryWriter(logs.format(lengh_msg))
    running_reward = np.ones(len(env.agent_index_list))
    actions = np.ones(len(env.agent_index_list))
    msgs = np.zeros((len(env.agent_index_list),9))


    t = 0
    info = np.zeros(7)
    # run inifinitely many episodes
    for i_episode in count(1):

        # reset environment and episode reward
        print("reset enviroment")
        state = env.reset()
        ep_reward = np.zeros(len(env.agent_index_list))
        memory = []
        # for each episode, only run 9999 steps so that we don't
        # infinite loop while learning
        for t in range(1, 300):


            for i in env.agent_index_list:

                if (1 - state[i, 0, 3]) < 0.6:
                    actions[i] = 1
                else:
                    actions[i] = np.random.randint(1, 4)

                # select action from policy

                actions_histogram[i].append(actions[i])

            state, reward, done, info = env.step(actions, msgs)

            memory.append(state[:, 0, 3])

            ep_reward += reward

            if done:
                break

        # update cumulative reward
        running_reward = 0.05 * ep_reward + (1 - 0.05) * running_reward

        mean_departured_pkgs.append(info[5])
        # log results
        if i_episode % 1 == 0:
            print('Episode {}\tsteps: {}\tLast reward: {:.2f}\tAverage reward: {:.2f}'.format(
                  i_episode, t, ep_reward.sum(), running_reward.sum()))
            writer.add_scalar("mean_reward", running_reward.sum(), i_episode)
            writer.add_scalar("episode_reward", ep_reward.sum(), i_episode)
            memory_np = np.array(memory)


            for agent_id in env.agent_index_list:

                writer.add_scalar("total_memory_used_agent_{}".format(agent_id), 100-100*memory_np[:, agent_id].mean(), i_episode)

            writer.add_scalar("total number of refused pkgs", info[4], i_episode)
            writer.add_scalar("total number of expired pkgs", info[2], i_episode)
            writer.add_scalar("total number of rerouted pkgs", info[3], i_episode)
        mean_departured_pkgs_value = np.array(mean_departured_pkgs[-50:]).mean()
        print("Stop condition: MAX Packages", opt.max_env_traffic)
        print("Number of packages delivered", info[5])
        print("50 mean Number of packages delivered", mean_departured_pkgs_value)

        # check if we have "solved" the servers challenge
        if i_episode > 10000:
            print("Solved! Running reward is now {} and "
                  "the last episode old_runs to {} time steps!".format(running_reward.sum(), t))


            writer.add_scalar("mean_reward", running_reward.sum(), i_episode)
            writer.add_scalar("episode_reward", ep_reward.sum(), i_episode)

            break

        writer.add_scalar("Mean total number of departured pkgs", mean_departured_pkgs_value, i_episode)
        writer.add_scalar("total number of departured pkgs", info[5], i_episode)
        writer.add_scalar("total number of refused pkgs", info[4], i_episode)
        writer.add_scalar("total number of expired pkgs", info[2], i_episode)
        writer.add_scalar("total number of rerouted pkgs", info[3], i_episode)





if __name__ == '__main__':
    main()
import numpy as np
import networkx as nx
from numpy.random import default_rng
import matplotlib.pyplot as plt

generator = default_rng()
# Environment Constants
IDLE = 0
BUSY = 1
FULL = 0
EMPTY = 1
MEMORY_AVAILABLE = 1
SEND_INFO = 1
HIDE_INFO = 0
EMPTY_MSG = -1


class Options:
    """
    Class that contains the environment configuration
    The user can choose its own set of configuration
    """
    def __init__(self,
                 number_servers=5,
                 max_size_input_queue=10,
                 livespan=10.,
                 memory=100,
                 speed=5,
                 pckg_size=(20, 50),
                 max_priority=3,
                 max_env_traffic=200,
                 max_discard_fc=0.1,
                 success_factor=0.3,
                 logs=False,
                 info_in_observation=5
                 ):
        self.print_info = logs
        self.info_in_observation = info_in_observation
        self.logs = logs
        self.max_size_input_queue = max_size_input_queue
        self.livespan = livespan
        self.memory = memory
        self.speed = speed
        self.pckg_size = pckg_size
        self.max_priority = max_priority
        self.number_servers = number_servers
        self.max_discard_packages = max_env_traffic * max_discard_fc
        self.max_env_traffic = max_env_traffic
        self.success_factor = success_factor


class Server:
    def __init__(self, server_id, server_neighbours, opt):
        self.info_obs = np.array([])
        self.logs = opt.logs
        self.server_id = server_id
        self.server_specs = {'memory': opt.memory, 'speed': opt.speed}
        self.processing_queue = []
        self.input_queue = []
        self.reward = 0
        self.total_reward = 0
        self.action = 0
        self.total_packages_processed = 0
        self.server_state = [IDLE,
                             len(self.input_queue),
                             EMPTY]  # machine status, elements input queue, memory_processing_queue

        self.rerouting_queue = []
        self.server_neighbours = server_neighbours  # one-hop server's neighbours
        self.drop_package = 0
        self.action_space = np.arange(5)
        self.message_space = [[HIDE_INFO, SEND_INFO] for _ in range(opt.info_in_observation + 4)]
        self.observation = np.zeros(opt.info_in_observation + 4)
        self.opt = opt
    def action_space_sample(self):
        action = np.random.choice(self.action_space)
        return action

    def message_space_sample(self):
        msg = [np.random.choice(msg_unit) for msg_unit in self.message_space]
        return msg

    def send_msg(self, msg_encoding):

        msg = -1 * np.ones(len(self.message_space))

        for index, msg_value in enumerate(msg_encoding):
            if msg_value > 0:
                msg[index] = self.observation[index]

        return (self.server_id, msg)

    def calculate_number_high_priority(self, queue):
        number_of_high_priority = 0
        for package in queue:
            if package["priority"] == 3:
                number_of_high_priority += 1

        return number_of_high_priority


    def get_obs(self, queue, num_arrivals, num_departs, total_expired, total_reroute, total_refuse):
        self.info_obs = np.array([num_arrivals, num_departs, total_expired, total_reroute, total_refuse])
        self.input_queue = queue
        number_of_high_priority = self.calculate_number_high_priority(queue)
        self.server_state[1] = len(self.input_queue)
        self.observation = np.array([number_of_high_priority,
                                     self.server_state[0],
                                     self.server_state[1],
                                     self.server_state[2],
                                     num_arrivals,
                                     num_departs,
                                     total_expired,
                                     total_reroute,
                                     total_refuse])

    def combine_info_obs(self, msg_array):
        new_obs = [self.observation]
        for msg in msg_array:
            new_obs.append(msg[0])
        return new_obs

    def update_process_queue(self, num_in_system, num_departs, total_wait):

        queue = self.processing_queue
        new_queue = []

        for package in queue:

            if package['depart_time'] > 0:
                package['depart_time'] -= 1
                new_queue.append(package)
                total_wait += 1
                num_in_system -= 1

            else:
                if self.logs:
                    print("[INFO] server {} has processed package".format(self.server_id))
                total_wait -= 1
                # num_in_system -= 1
                num_departs += 1
                self.server_state[2] = self.calculate_memory_state()

                if package["priority"] == 1:
                    self.reward += 3
                elif package["priority"] == 2:
                    self.reward += 4
                else:
                    self.reward += 7

        if len(new_queue) == 0:
            if self.logs:
                print("[INFO] server {} Memory is Empty, Machine is IDLE".format(self.server_id))
            self.server_state[0] = IDLE
            self.server_state[2] = EMPTY

        self.processing_queue = new_queue

        return num_in_system, num_departs, total_wait

    def reroute_package(self, to_id):

        re_route_package = self.input_queue[0]
        destination_server_id = int(to_id.split('_')[1])
        if self.logs:
            print("[INFO] from server {} re_routing to server {}".format(self.server_id, to_id))
        self.rerouting_queue.append([re_route_package, destination_server_id])

    def calculate_memory_state(self):
        # Keep it binary
        memory_used = 0
        memory_state = EMPTY
        next_input_package_size = 0

        # if self.input_queue:
        #     next_input_package_size = self.input_queue[0]['size']

        if self.processing_queue:

            for package in self.processing_queue:
                memory_used += package['size']

            # if self.server_specs["memory"] - memory_used > next_input_package_size:
            memory_state = float((self.server_specs["memory"] - memory_used)/self.server_specs["memory"])

            # else:
            #     memory_state = 0

        return memory_state

    def calculate_memory_available(self, memory_need):
        # Keep it binary
        memory_used = 0
        if self.processing_queue:

            for package in self.processing_queue:
                memory_used += package['size']
        return self.server_specs["memory"] - memory_used < memory_need

    def calculate_departure_time(self, pkg_size, server_speed):

        depart_time = pkg_size / server_speed

        return round(depart_time)

    def server_state_action_reward(self, action, num_in_system):
        #TODO TEST: REMOVE ERROR FOR USELESS ACTION
        reward = 0

        if action == 1:  # Accept Package
            if self.logs:
                print("[INFO] server {} Accepts input_package ".format(self.server_id))

            if self.server_state[2] == FULL:
                if self.logs:
                    print("[ERROR] server {} cannot accept package, Memory full or package is too big".format(self.server_id))
                reward = 0
                self.server_state = self.server_state

            elif self.input_queue:
                new_package = self.input_queue[0]
                memory_need = new_package['size']
                if new_package['size'] > self.server_specs["memory"] or self.calculate_memory_available(memory_need):
                    if self.logs:
                        print("[ERROR] server {} cannot accept package, input package is too big for server ".format(self.server_id))
                    # test change
                    reward = 0
                    self.server_state = self.server_state
                else:
                    new_package['depart_time'] = self.calculate_departure_time(new_package['size'],
                                                                               self.server_specs["speed"])
                    self.processing_queue.append(new_package)
                    self.input_queue = self.input_queue[1:]
                    reward = 5
                    memory_state = self.calculate_memory_state()
                    self.server_state = [BUSY, len(self.input_queue), memory_state]

            else:
                if self.logs:
                    print("[ERROR] server {} cannot accept package, input queue is empty ".format(self.server_id))
                # test change
                reward = 0
                # reward = 0
                self.server_state = self.server_state

        if action == 2:  # Reroute Package Right

            if self.input_queue:
                machine_state, queue_lenght, memory_state = self.server_state
                self.reroute_package(self.server_neighbours[0])
                self.input_queue = self.input_queue[1:]
                self.server_state = [machine_state, len(self.input_queue), memory_state]
                reward = 1  # Revisit depending on behaviour

            else:
                if self.logs:
                    print("[ERROR] server {} cannot reroute package, input queue is empty ".format(self.server_id))
                # test change
                # reward = -1
                reward = 0
                self.server_state = self.server_state

        if action == 3:  # Reroute Package Left

            if self.input_queue:
                machine_state, queue_lenght, memory_state = self.server_state
                self.reroute_package(self.server_neighbours[1])
                self.input_queue = self.input_queue[1:]
                self.server_state = [machine_state, len(self.input_queue), memory_state]
                reward = 1

            else:
                if self.logs:
                    print("[ERROR] server {} cannot reroute package, input queue is empty ".format(self.server_id))
                # test change
                # reward = -1
                reward = 0
                self.server_state = self.server_state

        if action == 4:  # Refuse Package
            if not self.input_queue:
                # test change
                # reward = -5
                reward = 0
                self.server_state = self.server_state
                if self.logs:
                    print("[ERROR] server {} cannot refuse package, input queue is empty ".format(self.server_id))
            else:
                machine_state, queue_lenght, memory_state = self.server_state
                reward = 0
                # drop_package = self.input_queue[0]
                # drop_package["livespan"] = self.opt.livespan
                self.input_queue = self.input_queue[1:]
                # self.input_queue.append(drop_package)
                self.server_state = [machine_state, len(self.input_queue), memory_state]
                self.drop_package += 1
                num_in_system -= 1


        if action == 0:
            # test change
            # reward = -1
            reward = 0  # NONE
            self.server_state = self.server_state

        self.reward = reward
        self.action = action

        return self.input_queue, self.reward, num_in_system


class MultiServerEnvironment:
    def __init__(self, opt):
        self.info_in_observation = opt.info_in_observation
        self.logs = opt.logs
        self.global_score = 0
        self.steps = 0
        self.msg_buffer = [[] for _ in range(opt.number_servers)]
        self.agent_index_list = np.array(range(opt.number_servers))
        self.number_nodes = opt.number_servers
        self.num_in_system = np.zeros(opt.number_servers)
        self.num_arrivals = np.zeros(opt.number_servers)
        self.num_departs = np.zeros(opt.number_servers)
        self.total_wait = np.zeros(opt.number_servers)
        self.total_expired = np.zeros(opt.number_servers)
        self.total_reroute = np.zeros(opt.number_servers)
        self.total_refuse = np.zeros(opt.number_servers)
        self.rewards = np.zeros(opt.number_servers)
        self.full_observation = np.zeros((opt.number_servers, 3, self.info_in_observation + 4))
        self.full_observation[:, 1:, :] = -1 * np.ones(self.info_in_observation + 4)
        self.full_observation[:, 0, 3] = 1
        self.livespan = opt.livespan
        self.opt = opt
        self.queues = [[] for _ in range(opt.number_servers)]
        self.print_info = opt.print_info
        self.G = nx.Graph()
        self.G.add_nodes_from(['node_{}'.format(i) for i in range(opt.number_servers)])
        list_agents = list(self.G.nodes)
        self.G.add_edges_from([(list_agents[i - 1], list_agents[i]) for i in range(opt.number_servers)])

        # init server and stack them into list
        agents_neighbours_list = [list(self.G.neighbors(i)) for i in list_agents]
        self.agents_neighbours_list = agents_neighbours_list
        self.Servers = [Server('node_{}'.format(i), agents_neighbours_list[i], opt) for i in range(opt.number_servers)]

    def process_msg(self, node_id):

        info = []

        for msg in self.msg_buffer[node_id]:
            if self.print_info:
                print("node {} has recieved {} from {}".format(node_id, msg[1], msg[0]))
            info.append([msg[1]])

        self.msg_buffer[node_id] = []

        return info

    def end_episode(self, rewards):

        total_dispatched_pkgs = self.total_expired.sum() + self.num_departs.sum() + self.total_refuse.sum()
        print("total_dispatched_pkgs: ",total_dispatched_pkgs)

        #TODO TEST
        total_dropped_pkgs = self.total_expired.sum() + self.total_refuse.sum()
        print("total_dropped_pkgs:", total_dropped_pkgs)
        # total_dropped_pkgs = self.total_expired.sum()
        if total_dropped_pkgs >= self.opt.max_discard_packages:
            print("end of episode, System Overload")
            for agent_index in self.agent_index_list:
                rewards[agent_index] = -50
            output = [1, rewards]

        elif total_dispatched_pkgs >= self.opt.max_env_traffic:
            print("end of episode, All packages were sucessfully processed")
            for agent_index in self.agent_index_list:
                rewards[agent_index] = 100

            output = [1, rewards]

        else:
            output = [0, rewards]

        return output

    def handle_queue(self, agent_id):
        queue = self.queues[agent_id]
        new_queue = []

        for package in queue:
            if package['livespan'] > 0:
                package['livespan'] -= 1
                new_queue.append(package)
            else:
                self.total_expired[agent_id] += 1
                self.num_in_system[agent_id] -= 1
                self.Servers[agent_id].reward -= 3

        self.queues[agent_id] = new_queue

    def generate_package(self, agent_id, opt):
        self.num_in_system[agent_id] += 1
        self.num_arrivals[agent_id] += 1

        size_package = np.random.randint(opt.pckg_size[0], opt.pckg_size[1])

        # priority_package = np.random.randint(1, opt.max_priority)

        self.queues[agent_id].append({"livespan": opt.livespan,
                                      "priority": self.claculate_priority(opt.livespan),
                                      "size": size_package,
                                      "depart_time": float("inf")})

    def claculate_priority(self, livespan):

        priority = 1
        if self.livespan/2 >= livespan > self.livespan/3:
            priority = 2
        elif livespan <= self.livespan/3:
            priority = 3

        return priority

    def update_priority(self, agent_id):
        queue = self.queues[agent_id]
        for package in queue:
            package["priority"] = self.claculate_priority(package["livespan"])
        return queue


    def handle_reroute_package(self, new_queues):
        # 2 queues can used
        servers = self.Servers
        queues = new_queues
        n_system = self.num_in_system

        for server_id, server in enumerate(servers):
            for routing_pkg, to_id in server.rerouting_queue:
                queues[to_id].append(routing_pkg)

                n_system[server_id] -= 1
                n_system[to_id] += 1

                servers[to_id].input_queue = queues[to_id]
                self.total_reroute[to_id] += 1

            server.rerouting_queue = []

        return servers, queues, n_system

    def random_traffic_flag_generator(self):
        return generator.binomial(1, self.opt.success_factor, 1)

    def reset(self):
        """
        restarting environment variables
        """
        self.info_in_observation = self.opt.info_in_observation
        self.logs = self.opt.logs
        self.global_score = 0
        self.steps = 0
        self.msg_buffer = [[] for _ in range(self.opt.number_servers)]
        self.agent_index_list = np.array(range(self.opt.number_servers))
        self.number_nodes = self.opt.number_servers
        self.num_in_system = np.zeros(self.opt.number_servers)
        self.num_arrivals = np.zeros(self.opt.number_servers)
        self.num_departs = np.zeros(self.opt.number_servers)
        self.total_wait = np.zeros(self.opt.number_servers)
        self.total_expired = np.zeros(self.opt.number_servers)
        self.total_reroute = np.zeros(self.opt.number_servers)
        self.total_refuse = np.zeros(self.opt.number_servers)
        self.rewards = np.zeros(self.opt.number_servers)
        self.full_observation = np.zeros((self.opt.number_servers, 3, self.info_in_observation + 4))
        self.full_observation[:, 1:, :] = -1 * np.ones(self.info_in_observation + 4)
        self.full_observation[:, 0, 3] = 1
        self.livespan = self.opt.livespan
        self.queues = [[] for _ in range(self.opt.number_servers)]
        self.print_info = self.opt.print_info
        self.G = nx.Graph()
        self.G.add_nodes_from(['node_{}'.format(i) for i in range(self.opt.number_servers)])
        list_agents = list(self.G.nodes)
        self.G.add_edges_from([(list_agents[i - 1], list_agents[i]) for i in range(self.opt.number_servers)])

        # init server and stack them into list
        agents_neighbours_list = [list(self.G.neighbors(i)) for i in list_agents]
        self.agents_neighbours_list = agents_neighbours_list
        self.Servers = [Server('node_{}'.format(i), agents_neighbours_list[i], self.opt) for i in range(self.opt.number_servers)]


        self.queues = [[] for _ in range(self.opt.number_servers)]

        self.G = nx.Graph()
        self.G.add_nodes_from(['node_{}'.format(i) for i in range(self.opt.number_servers)])
        list_agents = list(self.G.nodes)
        self.G.add_edges_from([(list_agents[i - 1], list_agents[i]) for i in range(self.opt.number_servers)])

        # init server and stack them into list
        agents_neighbours_list = [list(self.G.neighbors(i)) for i in list_agents]
        self.Servers = [Server('node_{}'.format(i), agents_neighbours_list[i], self.opt) for i in
                        range(self.opt.number_servers)]

        return self.full_observation.astype(np.float32)

    def step(self, actions=None, messages=None):
        done, self.rewards = self.end_episode(self.rewards)
        # Radomize agent calls
        np.random.shuffle(self.agent_index_list)

        # generate and handle (live-span decay) input packages
        for agent_id in self.agent_index_list:

            generate_package = self.random_traffic_flag_generator()
            self.handle_queue(agent_id)

            cond_stop_1 = len(self.queues[agent_id]) < self.opt.max_size_input_queue
            cond_stop_2 = self.num_arrivals.sum() < self.opt.max_env_traffic

            if (generate_package and cond_stop_1) and cond_stop_2:
                self.generate_package(agent_id, self.opt)


        # agents sample observation from environment
        for agent_id in self.agent_index_list:

            self.Servers[agent_id].get_obs(self.queues[agent_id],
                                           self.num_arrivals[agent_id],
                                           self.num_departs[agent_id],
                                           self.total_expired[agent_id],
                                           self.total_reroute[agent_id],
                                           self.total_refuse[agent_id])

        # agents send msg to their neighbours
        for agent_id in self.agent_index_list:
            agent_msg = messages[agent_id]
            agents_neighbours = list(self.G.neighbors(self.Servers[agent_id].server_id))

            for neighbour in agents_neighbours:
                neighbour_id = int(neighbour.split("_")[1])
                self.msg_buffer[neighbour_id].append(self.Servers[agent_id].send_msg(agent_msg))

        # agents apply actions
        for agent_id in self.agent_index_list:
            agent_action = actions[agent_id]
            self.queues[agent_id], reward, self.num_in_system[agent_id] = self.Servers[agent_id].server_state_action_reward(agent_action,self.num_in_system[agent_id])
            self.rewards[agent_id] = reward
            self.total_refuse[agent_id] = self.Servers[agent_id].drop_package

        # handle server's re-routing
        new_queues = self.queues

        self.Servers, self.queues, self.num_in_system = self.handle_reroute_package(new_queues)


        # handle server's internal queues
        for agent_id in self.agent_index_list:
            n_system, n_departs, n_wait = self.Servers[agent_id].update_process_queue(
                self.num_in_system[agent_id],
                self.num_departs[agent_id],
                self.total_wait[agent_id]
            )

            self.num_in_system[agent_id] = n_system
            self.num_departs[agent_id] = n_departs
            self.total_wait[agent_id] = n_wait

        for agent_id in self.agent_index_list:
            self.queues[agent_id] = self.update_priority(agent_id)

        # Extract msg information and stack to observation array
        for agent_id in self.agent_index_list:
            neighbours_info = self.process_msg(agent_id)
            full_obs = self.Servers[agent_id].combine_info_obs(neighbours_info)
            self.full_observation[agent_id] = np.array(full_obs)



        self.steps += 1

        info = [self.num_in_system.sum(),
                self.total_wait.sum(),
                self.total_expired.sum(),
                self.total_reroute.sum(),
                self.total_refuse.sum(),
                self.num_departs.sum(),
                self.num_arrivals.sum()]

        return self.full_observation.astype(np.float32), self.rewards.astype(np.float32), done, info

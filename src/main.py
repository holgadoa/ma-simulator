from src.enviroment.enviroment import *


def main():
    opt = Options()
    env = MultiServerEnvironment(opt)
    print("observation", env.reset().shape)
    actions = np.zeros(len(env.agent_index_list))
    msgs = np.zeros((len(env.agent_index_list), opt.info_in_observation + 3))
    done = 0
    while not done:
        step = env.steps
        print("-------------------- step {}".format(step))
        for i in env.agent_index_list:
            actions[i] = env.Servers[i].action_space_sample()
            # msgs[i] = env.Servers[i].message_space_sample()

        full_observation, rewards, done, info = env.step(actions, msgs)
        # print("actions:", actions)
        # print("full observation: obs+msgs", full_observation)
        # print("rewards", rewards)
        # print("done", done)
        print("num_in_system",info[0],
                "total_wait",info[1],
                "total_expired",info[2],
                "total reoute", info[3],
                "total_refuse",info[4],
                "num_departs", info[5],
                "num_arrivals", info[6])


if __name__ == "__main__":
    main()

